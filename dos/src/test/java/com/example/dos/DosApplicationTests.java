package com.example.dos;

import com.example.dos.common.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DosApplicationTests {
    private static final String PATTERN = "^[A-Za-z0-9-+\\u4e00-\\u9fa5]*$";

    @Test
    public void contextLoads() {
        if (!Pattern.matches(PATTERN,"张飞")){
            System.out.println("22");
        }else{
            System.out.println("111");
        }
    }

}
