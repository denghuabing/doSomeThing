package com.example.dos.controller;

import com.example.dos.domain.entity.JSONBeanResult;
import com.example.dos.domain.entity.User;
import com.example.dos.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class LoginController {

    private final static String LOGIN_PAGE = "/user/login";

    private final static String REGISTER_PAGE = "/user/register";

    private final static String INDEX_PAGE = "/index";

    private final static String ERROR_PAGE = "/user/404";

    @Autowired
    private UserService userService;

    @RequestMapping("/toRegister")
    public String registerPage(){
        return REGISTER_PAGE;
    }

    @RequestMapping("/register")
    public String register(User user,Map<String,String> map){
        boolean flag = userService.register(user);
        if(flag){
            return LOGIN_PAGE;
        }
        map.put("msg","注册失败");
        return REGISTER_PAGE;
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String toLogin(){
        return "/user/login";
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(Map<String, String> map,String username,String password)throws Exception{
        System.out.println("HomeController.login()");
        Subject currentUser = SecurityUtils.getSubject();
        String msg = "";
//        // 2、判断当前用户是否登录
//        if (currentUser.isAuthenticated() == false) {
//
//        }
        // 3、将用户名和密码封装到UsernamePasswordToken
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        // 4、认证
        try {
            currentUser.login(token);// 传到MyAuthorizingRealm类中的方法进行认证
            Session session = currentUser.getSession();
            session.setAttribute("userName", map.get("username"));
            return "/index";
        }catch (UnknownAccountException e){
            msg = "UnknownAccountException -- > 账号不存在：";
        }catch (IncorrectCredentialsException e){
            msg = "IncorrectCredentialsException -- > 密码不正确：";
        }catch (AuthenticationException e) {
            e.printStackTrace();
            msg="用户验证失败";
        }
        map.put("msg",msg);
        return LOGIN_PAGE;
    }

    @RequestMapping("/index")
    public String index(){
        return INDEX_PAGE;
    }

    @RequestMapping("/404")
    public String unauthorizedRole(){
        System.out.println("------没有权限-------");
        return ERROR_PAGE;
    }

    @RequestMapping("/logout")
    public String logout(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return LOGIN_PAGE;
    }
}
