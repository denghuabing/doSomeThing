package com.example.dos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/home")
public class HomeController {

    private static final String HOME_PAGE = "home";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(){
        return HOME_PAGE;
    }
}
