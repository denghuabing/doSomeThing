package com.example.dos;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(value = "com.example.dos.domain.dao")
public class DosApplication {

    public static void main(String[] args) {
        SpringApplication.run(DosApplication.class, args);
    }
}
