package com.example.dos.service;

import com.example.dos.domain.entity.JSONBeanResult;
import com.example.dos.domain.entity.Permissions;
import com.example.dos.domain.entity.User;

import java.util.List;

public interface UserService {

    User addUser(User user);

    User findUserByName(String username);

    Permissions findPermissionByUsername(String username);

    JSONBeanResult repetitionName(String name);

    List<User> findAllUser();
}
