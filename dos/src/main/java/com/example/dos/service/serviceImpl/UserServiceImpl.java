package com.example.dos.service.serviceImpl;

import com.example.dos.domain.dao.UserDao;
import com.example.dos.domain.entity.JSONBeanResult;
import com.example.dos.domain.entity.Permissions;
import com.example.dos.domain.entity.User;
import com.example.dos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User addUser(User user) {
        return null;
    }

    @Override
    public User findUserByName(String username){
        return userDao.findUserByName(username);
    }

    @Override
    public Permissions findPermissionByUsername(String username) {
        return userDao.findPermissionByUsername(username);
    }

    @Override
    public JSONBeanResult repetitionName(String name) {
        return new JSONBeanResult(false);
    }

    @Override
    public List<User> findAllUser() {
        return userDao.findAllUser();
    }
}
