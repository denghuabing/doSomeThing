package com.example.dos.domain.entity;

import lombok.Data;

@Data
public class Permissions {

    //权限编码
    private Integer permissionsCode;

    //权限名称
    private String permissionsName;

    //描述
    private String description;

    //是否可编辑  1可编辑 2不可编辑
    private Integer editable;
}
