package com.example.dos.domain.entity;

import java.io.Serializable;

public class JSONBeanResult implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final boolean SUCCESS = true;
    public static final boolean FALSE = false;

    private boolean success;

    //处理消息
    private String msg;

    //处理结果
    private Object obj;

    public JSONBeanResult(Object obj){
        this.success = true;
        this.obj = obj;
    }

    public JSONBeanResult(String msg){
        this.success = true;
        this.msg = msg;
    }

    public JSONBeanResult(boolean bol){
        this.success = bol;
    }

    public boolean isSuccess(){
        return this.success;
    }

    public Object getObj(){
        return this.obj;
    }
}
