package com.example.dos.domain.dao;

import com.example.dos.domain.entity.Permissions;
import com.example.dos.domain.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {

    List<User> findAllUser();

    User findUserByName(String username);

    Permissions findPermissionByUsername(String username);

    String getPassword(String username);

    String getRole(String username);

    boolean register(User user);

}
