package com.example.dos.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class User extends BasicBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    //权限(1、admin 2、manager(管理) 3、tourist(游客))
    private Integer permissionsCode = 3;

    //昵称
    private String nickname;

    private String phone;

    private String email;

}
