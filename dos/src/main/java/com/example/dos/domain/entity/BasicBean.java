package com.example.dos.domain.entity;

import com.example.dos.common.Utils;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
public class BasicBean {

    protected String id = Utils.getUUID();

    protected Date createDateTime = new Date();

    protected String createDateUsername;

    protected Date updateDateTime = new Date();

    protected String updateDateUsername;
}
